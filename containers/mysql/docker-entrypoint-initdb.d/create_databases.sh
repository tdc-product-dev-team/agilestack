#!/bin/bash
set -e

mysql --protocol=socket -uroot -p"${MYSQL_ROOT_PASSWORD}" <<-EOSQL
    CREATE DATABASE IF NOT EXISTS mattermost;
    GRANT ALL ON mattermost.* TO '${MYSQL_USER}'@'%';
EOSQL

