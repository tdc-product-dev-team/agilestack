#!/bin/sh

trap "exit" INT TERM
trap "kill 0" EXIT

ENV_NAMES='\
$JIRA_PROXY_SCHEME \
$JIRA_PROXY_NAME \
$JIRA_PROXY_PORT \
$CONFLUENCE_PROXY_SCHEME \
$CONFLUENCE_PROXY_NAME \
$CONFLUENCE_PROXY_PORT \
$BITBUCKET_PROXY_SCHEME \
$BITBUCKET_PROXY_NAME \
$BITBUCKET_PROXY_PORT \
$JENKINS_PROXY_SCHEME \
$JENKINS_PROXY_NAME \
$JENKINS_PROXY_PORT \
$SONARQUBE_PROXY_SCHEME \
$SONARQUBE_PROXY_NAME \
$SONARQUBE_PROXY_PORT \
$MATTERMOST_PROXY_SCHEME \
$MATTERMOST_PROXY_WS_SCHEME \
$MATTERMOST_PROXY_NAME \
$MATTERMOST_PROXY_PORT \
'

enable_proxy_conf() {
    envsubst "$ENV_NAMES" < /etc/nginx/conf.d/proxy-$1.template > /etc/nginx/conf.d/proxy-$1.conf
}

if [ "$CERTBOT_ENABLED" = "true" ]; then
    # Enable certbot configuration file
    envsubst "$ENV_NAMES" < /etc/nginx/conf.d/certbot.template > /etc/nginx/conf.d/certbot.conf

    # Start nginx
    nginx -g "daemon off;" &
    export NGINX_PID=$!

    # Get certifications
    /scripts/run_certbot.sh

    # Enable proxy configuration files
    if [ -n "$JIRA_PROXY_NAME" ]; then
        enable_proxy_conf jira-ssl
    fi

    if [ -n "$CONFLUENCE_PROXY_NAME" ]; then
        enable_proxy_conf confluence-ssl
    fi

    if [ -n "$BITBUCKET_PROXY_NAME" ]; then
        enable_proxy_conf bitbucket-ssl
    fi

    if [ -n "$JENKINS_PROXY_NAME" ]; then
        enable_proxy_conf jenkins-ssl
    fi

    if [ -n "$SONARQUBE_PROXY_NAME" ]; then
        enable_proxy_conf sonarqube-ssl
    fi

    if [ -n "$MATTERMOST_PROXY_NAME" ]; then
        enable_proxy_conf mattermost-ssl
    fi

    # Reload configuration files
    kill -HUP $NGINX_PID

    # Regularly renew certificates
    while [ true ]; do
        sleep 604800 &
        SLEEP_PID=$!

        /scripts/run_certbot.sh

        wait "$SLEEP_PID"
    done
else
    # Enable proxy configuration files
    if [ -n "$JIRA_PROXY_NAME" ]; then
        enable_proxy_conf jira
    fi

    if [ -n "$CONFLUENCE_PROXY_NAME" ]; then
        enable_proxy_conf confluence
    fi

    if [ -n "$BITBUCKET_PROXY_NAME" ]; then
        enable_proxy_conf bitbucket
    fi

    if [ -n "$JENKINS_PROXY_NAME" ]; then
        enable_proxy_conf jenkins
    fi

    if [ -n "$SONARQUBE_PROXY_NAME" ]; then
        enable_proxy_conf sonarqube
    fi

    if [ -n "$MATTERMOST_PROXY_NAME" ]; then
        enable_proxy_conf mattermost
    fi

    # Start nginx
    nginx -g "daemon off;"
fi

