#!/bin/sh

get_certificate() {
    echo "Getting certificates"
    echo "Domain options is $1"
    echo "Email option is $2"
    certbot certonly \
        --agree-tos \
        --keep \
        -n \
        --text \
        --email $2 \
        --server "https://acme-v02.api.letsencrypt.org/directory" \
        $1 \
        --cert-name agilestack \
        --http-01-port 1337 \
        --standalone \
        --preferred-challenges http-01 \
        --debug
}

if [ -z "$CERTBOT_EMAIL" ]; then
    echo "CERTBOT_EMAIL environment variable undefined. Certbot will do nothing."
    exit 1
fi

exit_code=0
set -x

# Get certificates
DOMAIN_OPTIONS=

add_domain_option() {
    if [ -n "$1" ]; then
        DOMAIN_OPTIONS="$DOMAIN_OPTIONS -d $1"
    fi
}

add_domain_option "$JIRA_PROXY_NAME"
add_domain_option "$CONFLUENCE_PROXY_NAME"
add_domain_option "$BITBUCKET_PROXY_NAME" 
add_domain_option "$JENKINS_PROXY_NAME"
add_domain_option "$SONARQUBE_PROXY_NAME"
add_domain_option "$MATTERMOST_PROXY_NAME"
get_certificate "$DOMAIN_OPTIONS" $CERTBOT_EMAIL

# Reload nginx configurations
kill -HUP $NGINX_PID

set +x
exit $exit_code

