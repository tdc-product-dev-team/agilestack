# Agilestack

This is software stack for agile project management.

The stack will be deployed with Docker Compose.

## Stack

| Service                        | Product                                                                                                                                    |
|--------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------|
| Project management             | [Jira](https://www.atlassian.com/software/jira) 7.13.1 [[Docker Image](https://hub.docker.com/r/cptactionhank/atlassian-jira-software/)]   |
| Wiki                           | [Confluence](https://www.atlassian.com/software/confluence) 6.11.0 [[Docker Image](https://hub.docker.com/r/atlassian/confluence-server/)] |
| Source code management         | [BitBucket](https://www.atlassian.com/software/bitbucket) 5.12.0 [[Docker Image](https://hub.docker.com/r/atlassian/bitbucket-server/)]    |
| Continuous integration         | [Jenkins](https://jenkins.io/) 2.138 [[Docker Image](https://hub.docker.com/r/jenkins/jenkins/)]                                           |
| Source code quality inspection | [SonarQube](https://www.sonarqube.org/) 7.1 [[Docker Image](https://hub.docker.com/_/sonarqube/)]                                          |
| Communication                  | [Mattermost](https://www.mattermost.org/) 5.2.0 [[Docker Image](https://hub.docker.com/r/mattermost/mattermost-prod-app/)]                 |
| Reverse proxy                  | [Nginx](https://www.nginx.com/) 1.14 [[Docker Image](https://hub.docker.com/_/nginx/)]                                                     |
| Internal database              | [PostgreSQL](https://www.postgresql.org/) 10.5 [[Docker Image](https://hub.docker.com/_/postgres/)]                                        |
| Internal database              | [MySQL](https://www.mysql.com/) 5.7.23 [[Docker Image](https://hub.docker.com/_/mysql/)]                                                   |

## Supported operating systems

* Ubuntu: =16.04
* CentOS: >=7.5.1804

## Software requirements

* Git: >=1.8.3.1
* Docker: >=18.06.0-ce
* Docker Compose: >=1.22.0

## Hardware requirements
### Memory

* Minimum: 16GB

### Disk

* Minimum: 20GB

## About Atlassian software license

The software stack is free but using Atlassian softwares (Jira, BitBucket, Confluence) on production environment require buying self-hosted software licenses.
Atlassian provides trial licenses that expires 90 days.

* [Jira | Pricing](https://www.atlassian.com/software/jira/pricing?tab=self-hosted)
* [BitBucket | Pricing](https://bitbucket.org/product/pricing)
* [Confluence | Pricing](https://www.atlassian.com/software/confluence/pricing?tab=self-hosted)

## Installation
### Install prerequirements

Install following packages.

* [Git](https://git-scm.com/download/linux)
* Docker
  [[Ubuntu](https://docs.docker.com/install/linux/docker-ce/ubuntu/)]
  [[CentOS](https://docs.docker.com/install/linux/docker-ce/centos/)]
* [Docker Compose](https://docs.docker.com/compose/install/)

### Edit environment variables

This stack requires some environment variables.
Following table is environment variables required and examples.

| Name                       | Description                                    | Value         | Example     |
|----------------------------|------------------------------------------------|---------------|-------------|
| JIRA_PROXY_SCHEME          | External access scheme for Jira                | http or https | http        |
| JIRA_PROXY_NAME            | External access host name for Jira             | any           | localhost   |
| JIRA_PROXY_PORT            | External access port for Jira                  | number        | 80          |
| CONFLUENCE_PROXY_SCHEME    | External access scheme for Confluence          | http or https | http        |
| CONFLUENCE_PROXY_NAME      | External access host name for Confluence       | any           | localhost   |
| CONFLUENCE_PROXY_PORT      | External access port for Confluence            | number        | 80          |
| BITBUCKET_PROXY_SCHEME     | External access scheme for BitBucket           | http or https | http        |
| BITBUCKET_PROXY_NAME       | External access host name for BitBucket        | any           | localhost   |
| BITBUCKET_PROXY_PORT       | External access port for BitBucket             | number        | 80          |
| JENKINS_PROXY_SCHEME       | External access scheme for Jenkins             | http or https | http        |
| JENKINS_PROXY_NAME         | External access host name for Jenkins          | any           | localhost   |
| JENKINS_PROXY_PORT         | External access port for Jenkins               | number        | 80          |
| JENKINS_JAVA_OPTS          | Jenkins JVM parameters                         | any           | -Dxxx=yyy   |
| SONARQUBE_PROXY_SCHEME     | External access scheme for SonarQube           | http or https | http        |
| SONARQUBE_PROXY_NAME       | External access host name for SonarQube        | any           | localhost   |
| SONARQUBE_PROXY_PORT       | External access port for SonarQube             | number        | 80          |
| MATTERMOST_PROXY_SCHEME    | External access scheme for SonarQube           | http or https | http        |
| MATTERMOST_PROXY_WS_SCHEME | External access websocket scheme for SonarQube | ws or wss     | ws          |
| MATTERMOST_PROXY_NAME      | External access host name for SonarQube        | any           | localhost   |
| MATTERMOST_PROXY_PORT      | External access port for SonarQube             | number        | 80          |
| CERTBOT_ENABLED            | Enable proxy TLS with certbot                  | true or false | false       |
| CERTBOT_EMAIL              | Email for getting TLS certificate              | email         | xxx@yyy.zzz |
| POSTGRES_PASSWORD          | PostgreSQL super user password                 | any           | postgres    |
| MYSQL_PASSWORD             | MySQL super user password                      | any           | mysql       |

Installation requires `.env` file. Following code is example `.env` file.

```
JIRA_PROXY_SCHEME=http
JIRA_PROXY_NAME=jira.example.com
JIRA_PROXY_PORT=80
CONFLUENCE_PROXY_SCHEME=http
CONFLUENCE_PROXY_NAME=confluence.example.com
CONFLUENCE_PROXY_PORT=80
BITBUCKET_PROXY_SCHEME=http
BITBUCKET_PROXY_NAME=bitbucket.example.com
BITBUCKET_PROXY_PORT=80
JENKINS_PROXY_SCHEME=http
JENKINS_PROXY_NAME=jenkins.example.com
JENKINS_PROXY_PORT=80
JENKINS_JAVA_OPTS=-Dorg.apache.commons.jelly.tags.fmt.timeZone=Asia/Tokyo
SONARQUBE_PROXY_SCHEME=http
SONARQUBE_PROXY_NAME=sonarqube.example.com
SONARQUBE_PROXY_PORT=80
MATTERMOST_PROXY_SCHEME=http
MATTERMOST_PROXY_WS_SCHEME=ws
MATTERMOST_PROXY_NAME=mattermost.example.com
MATTERMOST_PROXY_PORT=80
CERTBOT_ENABLED=false
CERTBOT_EMAIL=xxx@yyy.zzz
POSTGRES_PASSWORD=postgres
MYSQL_PASSWORD=mysql
```

### Enable TLS of reverse proxy

If `CERTBOT_ENABLED` environment variable is true then the container script will get TLS certificate when starting.

Please note the following points if enabling TLS.

* `*_SCHEME` environment variables must be `https`.
* `*_PORT` environment variables must be `443`.
* The containers must be accessible from the Internet.

### Install containers

Install container by `docker-compose`.

```bash
sudo docker-compose up -d
```

If you use example `.env` file then you can access services with below URLs.

| Service    | External access URL           | Internal access URL   | In containers URL      |
|------------|-------------------------------|-----------------------|------------------------|
| Jira       | http://jira.example.com       | http://localhost:8080 | http://jira:8080       |
| Confluence | http://confluence.example.com | http://localhost:8090 | http://confluence:8090 |
| BitBucket  | http://bitbucket.example.com  | http://localhost:7990 | http://bitbucket:7990  |
| Jenkins    | http://jenkins.example.com    | http://localhost:8081 | http://jenkins:8080    |
| SonarQube  | http://sonarqube.example.com  | http://localhost:9000 | http://sonarqube:9000  |
| Mattermost | http://mattermost.example.com | http://localhost:8000 | http://mattermost:8000 |

### Network port

| Port  | Service    | Scheme     |
|-------|------------|------------|
| 80    | Nginx      | HTTP       |
| 443   | Nginx      | HTTPS      |
| 8080  | Jira       | HTTP       |
| 8090  | Confluence | HTTP       |
| 7990  | BitBucket  | HTTP       |
| 7999  | BitBucket  | SSH        |
| 8081  | Jenkins    | HTTP       |
| 9000  | SonarQube  | HTTP       |
| 8000  | Mattermost | HTTP       |
| 5432  | PostgreSQL | PostgreSQL |

### Setup Jira

Access Jira URL with web browser then view install wizard.

Select production installation.

Enter following values at database configuration.

| Field         | Value                                          |
|---------------|------------------------------------------------|
| Database Type | `PostgreSQL`                                   |
| Hostname      | `postgres`                                     |
| Port          | `5432`                                         |
| Database Name | `jira`                                         |
| Username      | `postgres`                                     |
| Password      | `POSTGRES_PASSWORD` environment variable value |

Follow the installation wizard and enter the remaining settings.

### Setup Confluence

Access Confluence URL with web browser then view install wizard.

Select production installation.

Enter following values at database configuration.

| Field         | Value                                          |
|---------------|------------------------------------------------|
| Database Type | `PostgreSQL`                                   |
| Hostname      | `postgres`                                     |
| Port          | `5432`                                         |
| Database Name | `confluence`                                   |
| Username      | `postgres`                                     |
| Password      | `POSTGRES_PASSWORD` environment variable value |

Follow the installation wizard and enter the remaining settings.

### Setup BitBucket

Access BitBucket URL with web browser then view install wizard.

Select production installation.

Enter following values at database configuration.

| Field         | Value                                          |
|---------------|------------------------------------------------|
| Database Type | `PostgreSQL`                                   |
| Hostname      | `postgres`                                     |
| Port          | `5432`                                         |
| Database Name | `bitbucket`                                    |
| Username      | `postgres`                                     |
| Password      | `POSTGRES_PASSWORD` environment variable value |

Follow the installation wizard and enter the remaining settings.

### Setup Jenkins

Access Jenkins URL with web browser then view install wizard.

Jenkins installation requires initial admin password.
View initial admin password in jenkins container with following commands.

```bash
sudo docker exec -it $(sudo docker ps -f "name=jenkins" --format "{{.ID}}") cat /var/jenkins_home/secrets/initialAdminPassword
```

Follow the installation wizard and enter the remaining settings.

### Setup SonarQube

Access SonarQube URL with web browser then view homepage.

Initial password is serious security risk. Sonarqube password must be changed after first login.
Sonarqube initial ID/PW is admin/admin.

Further [Force user authentication must be enabled](https://docs.sonarqube.org/display/SONAR/Authentication).
