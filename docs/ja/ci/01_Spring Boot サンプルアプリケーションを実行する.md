# Spring Boot サンプルアプリケーションを実行する

このガイドでは、このプロジェクト・ファイルに含まれる Spring Boot サンプルアプリケーションを実行します。

## ローカル環境に必要なソフトウェアをインストール

* バージョン 1.8.0_181 以上の Oracle JDK をインストールします
* バージョン 3.5.4 以上の [Apache Maven](https://maven.apache.org/) をインストールします
* `mvn` コマンドが実行できるように [構成](https://maven.apache.org/install.html) します
* HTTP プロキシを経由する必要がある場合は Apache Maven のプロキシ設定を [構成](https://maven.apache.org/guides/mini/guide-proxies.html) します
* バージョン 10 以上の PostgreSQL をインストールします
* `spring_boot_sample` という名称のデータベースを PostgreSQL に作成します

## アプリケーションを実行

ローカル環境にて以下のコマンドを実行してください。

```bash
cd samples/spring-boot-sample
mvn spring-boot:run
```

コンソールに `Started Application` のログが出力された後に、Web ブラウザで [http://localhost:8080/](http://localhost:8080/) を開いてください。

Web ページが表示されれば成功です。

以上でこのガイドは終了です。
