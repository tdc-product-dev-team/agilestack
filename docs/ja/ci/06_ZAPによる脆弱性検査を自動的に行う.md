# ZAP による脆弱性検査を自動的に行う

このガイドでは Jenkins と [OWASP ZAP](https://www.owasp.org/index.php/OWASP_Zed_Attack_Proxy_Project) を使用して
Web アプリケーションに対する自動的な脆弱性検査を行うための方法を説明します。

このガイドは [A Step by step guide to integrate zap with jenkins](https://www.we45.com/blog/how-to-integrate-zap-into-jenkins-ci-pipeline-we45-blog) を元に作成しています。

## ゴール

*Continuous Integration* (CI) の中で脆弱性検査を行うことでセキュアなソフトウェアを提供するためのコストを低減することを目指します。
一般的にはこの手法を DevSecOps や Shift Left と呼びます。

## Jenkins に必要なプラグインをインストール

Web ブラウザで `https://JENKINS_HOSTNAME/pluginManager/available` を開き、[プラグインマネージャー] を表示してください。

以下のプラグインをインストールしてください。

* Official OWASP ZAP Jenkins Plugin
* Custom Tools Plugin
* HTML Publisher plugin

## ZAP 2.7.0 を Jenkins にインストール

Web ブラウザで `https://JENKINS_HOSTNAME/pluginManager/configureTools/` を開き、[Global Tool Configuration] を表示してください。

以下の手順で ZAP 2.7.0 を Jenkins にインストールします。

* [Custom tool] セクションの [Custom tool 追加] を押します
    * [名前] に `ZAP_2.7.0` を入力します
    * [アーカイブダウンロードURL] に `https://github.com/zaproxy/zaproxy/releases/download/2.7.0/ZAP_2.7.0_Linux.tar.gz` を入力します
    * [アーカイブを展開するサブディレクトリ] に `ZAP_2.7.0` を入力します
* [Apply] を押します

## Official OWASP ZAP Jenkins Plugin の設定

Web ブラウザで `https://JENKINS_HOSTNAME/configure` を開き、[システムの設定] を表示してください。

以下の手順で Official OWASP ZAP Jenkins Plugin を設定します。

* [ZAP] セクションに移動します
    * [Default Host] に `localhost` を入力します
    * [Default Port] に `8090` を入力します
* [Apply] を押します

## ZAP による脆弱性検査を行うジョブ設定

Web ブラウザで `https://JENKINS_HOSTNAME/view/all/newJob` を開き、[新規ジョブ作成] を表示してください。

* [Enter an item name] に `jenkins-zap-scan` を入力します
* [フリースタイル・プロジェクトのビルド] を選択ます
* [OK] を押します

Web ブラウザで `https://JENKINS_HOSTNAME/job/jenkins-zap-scan/` を開き、ジョブを表示してください。

* [ビルドの実行] を押します

Web ブラウザで `https://JENKINS_HOSTNAME/job/jenkins-zap-scan/configure` を開き、ジョブの設定を表示してください。

* [保存] を押します

再度、Web ブラウザで `https://JENKINS_HOSTNAME/job/jenkins-zap-scan/configure` を開き、ジョブの設定を表示してください。

ビルド環境

* [Install custom tools] をチェックします
* [Add Tool] を押します
* [Tool selection] で `ZAP_2.7.0` を選びます
* [Apply] を押します

ビルド

ZAP でスキャンする対象の URL を `<TARGET_URL>` と記載しています。
Jenkins コンテナから `<TARGET_URL>` に対して HTTP リクエストできるように構成する必要があります。

* [ビルド手順の追加] から [Execute ZAP] を追加します
* [Installation Method] セクションに移動します
* [Custom Tools Installation] を選びます
* [Name] で `ZAP_2.7.0` を選びます
* [Zap Home Directory] セクションに移動します
* [Path] に `/var/jenkins_home/.ZAP/` を入力します
* [Session Management] セクションに移動します
* [Persist Session] をチェックします
* [Filename] に `zap` を入力します
* [Session Properties] セクションに移動します
* [Context Name] に `zap` を入力します
* [Include in Context] に `<TARGET_URL>/*` を入力します
* [Attack Mode] セクションに移動します
* [Starting Point] に `<TARGET_URL>` を入力します
* [Spider Scan] をチェックします
* [Active Scan] をチェックします
* [Finalize Run] に移動します
* [Filename] に `JENKINS_ZAP_VULNERABILITY_REPORT_${BUILD_ID}` を入力します
* [Generate Reports] をチェックします
* [Format] で `html` を選びます
* [Apply] を押します

ビルド後の処理

* [ビルド後の処理の追加] から [成果物を保存] を追加します
* [保存するファイル] に `logs/*,reports/*` を入力します
* [ビルド後の処理の追加] から [Publish HTML reports] を追加します
* [追加] を押します
* [HTML directory to archive] に `reports/` を入力します
* [Index page[s]] に `JENKINS_ZAP_VULNERABILITY_REPORT_${BUILD_ID}.html` を入力します
* [Report title] に `ZAP HTML Report` を入力します
* [Apply] を押します

## ZAP のジョブを実行

Web ブラウザで `https://JENKINS_HOSTNAME/job/jenkins-zap-scan/` を開き、ジョブを表示してください。

* [ビルドの実行] を押します

ビルド成功後に Web ブラウザで `https://JENKINS_HOSTNAME/job/jenkins-zap-scan/ZAP_20HTML_20Report/` を開き [ZAP HTML Report] を表示してください。
