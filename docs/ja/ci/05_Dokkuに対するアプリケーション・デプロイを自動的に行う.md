# Dokku に対するアプリケーション・デプロイを自動的に行う

このガイドでは Dokku にアプリケーションをデプロイする方法を説明します。
Dokku は単一のサーバ上で動作するオープンソースのプラットフォームサービスです。

## ゴール

このガイドではソースコード品質チェックを自動的に行うことをゴールとします。

自動化された作業は以下のように動作します。

1. BitBucket の特定のブランチにソースコードを Push します
2. Push をトリガーとして Jenkins のデプロイ・ジョブを実行します
3. デプロイ・ジョブにおいて Dokku に対して Push します

## Dokku のインストール

Dokku のインストールを行います。
この手順は [Getting Started with Dokku](http://dokku.viewdocs.io/dokku/getting-started/installation/) を元にしています。

このガイドで使用するサーバは以下の通りです。

* オペレーティングシステム: Ubuntu 16.06
* メモリ: 2GB
* ハードディスク: 20GB

事前に Dokku にデプロイする際に使用する鍵ペアを作成してください。

もしローカル環境に [OpenSSL](https://www.openssl.org/) がインストールされている場合は以下のコマンドで鍵ペアを作成することができます。

```bash
# ローカル環境で実行します。
ssh-keygen -t rsa -b 2048
```

ローカル環境から Dokku に対して作成した鍵ペアを使って Git Push できるように構成する必要があります。
`~/.ssh/config` を以下のように編集してください。
`DOKKU_HOSTNAME` と `PRIVATE_KEY_FILE` は自身の環境に合わせて置き換えてください。

```
Host <DOKKU_HOSTNAME>
  User         dokku
  IdentityFile <PRIVATE_KEY_FILE>
```

Dokku 用サーバに Dokku v0.12.12 をインストールします。

```bash
# Dokkuサーバで実行します。
wget https://raw.githubusercontent.com/dokku/dokku/v0.12.12/bootstrap.sh
sudo DOKKU_TAG=v0.12.12 bash bootstrap.sh
```

数分後に Dokku のインストールが完了します。

次に Web インストーラを使用してセットアップを行います。
Web ブラウザで `http://DOKKU_HOSTNAME/` を開いて、[Dokku Setup] を表示します。

* [Public Key] に作成した鍵ペアのうち公開鍵のファイル (\*.pub) の内容をペーストします
* [Hostname] にデプロイしたアプリケーションにアクセスするホスト名を入力します
* [Use virtualhost naming for apps] をチェックします
* [Finish Setup] を押します

セットアップが完了すると Web インストーラが終了します。

## Dokku アプリケーションを作成

以下のコマンドを実行して、`spring-boot-sample` と `nuxtjs-sample` のアプリケーションを作成します。

```bash
# Dokkuサーバで実行します。
dokku apps:create spring-boot-sample
```

Dokku に Nuxt.js 用アプリを作成する際は環境変数の設定が追加で必要となります。
この手順は [How to deploy on Dokku?](https://nuxtjs.org/faq/dokku-deployment/) を元にしています。

```bash
# Dokkuサーバで実行します。
dokku apps:create nuxtjs-sample
dokku config:set nuxtjs-sample NPM_CONFIG_PRODUCTION=false
dokku config:set nuxtjs-sample HOST=0.0.0.0 NODE_ENV=production
```

## ローカル環境から Dokku にアプリケーションをデプロイ

Dokku にアプリケーションをデプロイするには Heroku と同様に Git を使用します。
この手順は [Deploying to Dokku](http://dokku.viewdocs.io/dokku~v0.12.12/deployment/application-deployment/) を元にしています。

このガイドではアプリケーションがサブディレクトリ内にあるため `subtree push` を実行していますが、本来は通常の `push` で構いません。

### Spring Boot アプリケーションをデプロイ

ローカル環境のプロジェクトのベースディレクトリにて以下のコマンドを実行してデプロイしてください。

```bash
# ローカル環境で実行します。
git remote add dokku-spring-boot-sample dokku@DOKKU_HOSTNAME:spring-boot-sample
git subtree push --prefix=samples/spring-boot-sample dokku-spring-boot-sample master
```

デプロイ完了後、Web ブラウザで `http://spring-boot-sample.DOKKU_HOSTNAME/` を開くと
Spring Boot アプリケーションの Web ページが表示されます。

### Nuxt.js アプリケーションをデプロイ

ローカル環境のプロジェクトのベースディレクトリにて以下のコマンドを実行してデプロイしてください。

```bash
# ローカル環境で実行します。
git remote add dokku-nuxtjs-sample dokku@DOKKU_HOSTNAME:nuxtjs-sample
git subtree push --prefix=samples/nuxtjs-sample dokku-nuxtjs-sample master
```

デプロイ完了後、Web ブラウザで `http://nuxtjs-sample.DOKKU_HOSTNAME/` を開くと
Nuxt.js アプリケーションの Web ページが表示されます。

## Jenkins に デプロイ・ジョブを登録する

Dokku にデプロイするジョブの構成手順を説明します。

以下の手順は Spring Boot アプリケーション向けのものです。
他のアプリケーション形式における設定内容は Spring Boot と同様であるため、このガイドでは省略します。

Web ブラウザで `https://JENKINS_HOSTNAME/view/all/newJob` を開き、[新規ジョブ作成] を表示してください。

* [Enter an item name] に `spring-boot-sample-deploy` を入力します
* [フリースタイル・プロジェクトのビルド] を選択します
* [OK] を押します

`https://JENKINS_HOSTNAME/job/spring-boot-sample-test/configure` を開き、ジョブの設定を表示してください。

ジョブの設定を行います。

ソースコード管理

* [Git] を選択します
* [リポジトリURL] に `https://BITBUCKET_HOSTNAME/scm/agil/agilestack.git` を入力します
* [認証情報] に BitBucket の ID/PW を追加します
* [Apply] を押します

ビルド・トリガ

* [SCMをポーリング] をチェックします
* [Apply] を押します

ビルド環境

* [秘密テキストや秘密ファイルを使用する] をチェックします
* [SSH User Private Key] のセクションにて以下のように設定します
    * [Key File Variable] に `DOKKU_KEY_FILE` を入力します
    * [認証情報] の [追加] を押します
        * [種類] は `SSH ユーザ名と秘密鍵` を選びます
        * [スコープ] は `グローバル` を選びます
        * [ユーザ名] に `dokku` を入力します
        * [秘密鍵] の [直接入力] をチェックします
        * [鍵] に作成した鍵ペアのうち秘密鍵の内容をペーストします
        * [追加] します
* [Apply] を押します

ビルド

* [ビルド手順の追加] を押して [シェルの実行] を選びます
* [シェルスクリプト] に以下のスクリプトを入力します

```bash
GIT_SSH_COMMAND="ssh -i $DOKKU_KEY_FILE -o StrictHostKeyChecking=no -F /dev/null" \
git subtree push --prefix=samples/spring-boot-sample dokku@DOKKU_HOSTNAME:spring-boot-sample master
```

* [Apply] を押します

Web ブラウザで `https://JENKINS_HOSTNAME/job/spring-boot-sample-deploy/` を開き、ジョブを表示してください。

[ビルド実行] を押します。

ビルドエラーが発生せずに完了すれば成功です。

ただし、ローカル環境でデプロイしてからソースコードの際は発生していないため、
この時点では特にデプロイは行われません。

[コンソール出力] には `Everything up-to-date` と出力され、特にデプロイが行われていないことが判ります。

## BitBucket にソースコードを Push

サンプルアプリケーションのコンテンツを修正して BitBucket に Push して、自動デプロイをテストします。

『ソースコード品質チェックを自動的に行う』のガイドで、すでに Push のウェブフックを作成しているため、
BitBucket 側に追加の設定は必要ありません。

今回は Spring Boot アプリケーションの `samples/spring-boot-sample/src/main/resources/templates/index.html` を編集してください。
編集内容は自由ですが、画面表示が変わる変更内容にしてください。

編集完了後、以下のコマンドを実行してください。

```bash
# ローカル環境で実行します。
git add .
git commit -m "Edit index.html"
git push agilestack master
```

BitBucket に Push すると Jenkins のデプロイ・ジョブが自動的に実行されます。

Web ブラウザで `http://spring-boot-sample.DOKKU_HOSTNAME/` を開いて表示内容が変われば成功です。


以上でこのガイドは終了です。
