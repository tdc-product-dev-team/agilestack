# Nuxt.js サンプルアプリケーションを実行する

このガイドでは、このプロジェクト・ファイルに含まれる Nuxt.js サンプルアプリケーションを実行します。

## ローカル環境に必要なソフトウェアをインストール

* バージョン 10.7.0 以上の [Node.js](https://nodejs.org) をインストールします
* HTTP プロキシを経由する必要がある場合は `npm` のプロキシ設定を [構成](https://docs.npmjs.com/misc/config) します

## アプリケーションを実行

ローカル環境にて以下のコマンドを実行してください。

```bash
cd samples/nuxtjs-sample
npm install
npm run dev
```

以下の URL を Web ブラウザで開き、ページが表示されることを確認してください。

コンソールに `Compiled successfully` のログが出力された後に、Web ブラウザで [http://localhost:3000/](http://localhost:3000/) を開いてください。

Web ページが表示されれば成功です。

このアプリケーションは `http://localhost:8080` で動作する Spring Boot アプリケーションの REST API を呼び出します。
[+] ボタンを押して行が追加されれば Nuxt.js アプリケーションと Spring Boot アプリケーションの連携が正常に動作しています。

以上でこのガイドは終了です。
