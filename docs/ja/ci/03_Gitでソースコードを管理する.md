# Git でソースコードを管理する

このガイドでは Git を使用してソースコードを管理する方法を説明します。

## ゴール

このガイドではソースコードを BitBucket に Push して、BitBucket 上でソースコードを確認することを目標とします。

## BitBucket のリポジトリを作成する

BitBucket にこのプロジェクト・ソースコードを Push するために、
[プロジェクト] と [リポジトリ] を作成する必要があります。

Web ブラウザで `https://BITBUCKET_HOSTNAME` を開き、管理権限を持つユーザでログインしてください。

[プロジェクト] を追加してください。

* [プロジェクト名] は `agilestack` を入力します
* [プロジェクトキー] は `AGIL` を入力します
* [プロジェクトを作成] を押します

`agilestack` プロジェクトに [リポジトリ] を追加してください。

* [リポジトリ名] は `agilestack` を入力します
* [リポジトリを作成] を押します

## BitBucket にソースコードを Push する

BitBucket にソースコードを Push します。

今回は Push の認証に BitBucket ユーザの ID/PW を使用します。

ローカル環境にて以下のコマンドを実行してください。

```bash
git remote add agilestack https://BITBUCKET_HOSTNAME/scm/aenv/agilestack.git
git push agilestack master
```

このガイドでは BitBucket 用の Git リモートに `agilestack` という名前を付けています。
ただし、通常の開発作業においてはメインの Git リモートに `origin` という名前を付けることに注意してください。

## BitBucket の Web サイトを表示する

Web ブラウザで `https://BITBUCKET_HOSTNAME/projects/AGIL/repos/agilestack/browse` を開き、リポジトリの内容を表示してください。

BitBucket の Web サイトでは Push されたファイルの内容やコミット履歴などを確認することができます。

以上でこのガイドは終了です。
