# SonarQube によるソースコード品質チェックを自動的に行う

このガイドではソースコードの自動的な品質チェックの構成方法を説明します。

## ゴール

このガイドではソースコード品質チェックを自動的に行うことをゴールとします。

自動化された作業は以下のように動作します。

1. BitBucket の特定のブランチにソースコードを Push します
2. Push をトリガーとして Jenkins のテスト・ジョブを実行します
3. テスト・ジョブにおいてユニット・テストおよび SonarQube による品質チェックを行います

上記の作業の結果、Jenkins のジョブ実行結果において SonarQube の [Quality Gate] を通過したか否か、
また SonarQube のプロジェクトにおいて品質チェックのレポートを参照することができます。

## Jenkins に必要なプラグインをインストール

Web ブラウザで `https://JENKINS_HOSTNAME/pluginManager/available` を開き、[プラグインマネージャー] を表示してください。

以下のプラグインをインストールしてください。

* Git plugin
* Maven Integration plugin
* NodeJS Plugin
* SonarQube Scanner for Jenkins

## SonarQube に必要なプラグインをインストール

Web ブラウザで `https://SONARQUBE_HOSTNAME/admin/marketplace` を開き、[Marketpalce] を表示してください。

以下の Feature をインストールしてください。

* SonarJS
* SonarJava

## Jenkins の Global Tool Configuration

Web ブラウザで `https://JENKINS_HOSTNAME/pluginManager/configureTools/` を開き、[Global Tool Configuration] を表示してください。

以下のように設定します。

### Git

既に Default という名称の [インストール済みGit] がある場合は、この手順をスキップできます。

* [Git追加] を押します
* [名前] に `Default` を入力します
* [Apply] を押します

### SonarQube Scanner

既に Default という名称の [インストール済みSonarQube Scanner] がある場合は、この手順をスキップできます。

* [SonarQube Scanner追加] を押します
* [名前] に `Default` を入力します
* [自動インストール] をチェックします
* [バージョン] は `3.2.0.1227` 以上を選択します
* [Apply] を押します

### Maven

既に Default という名称の [インストール済みMaven] がある場合は、この手順をスキップできます。

* [Maven追加] を押します
* [名前] に `Default` を入力します
* [自動インストール] をチェックします
* [バージョン] は `3.5.4` 以上を選択します
* [Apply] を押します

### NodeJS

既に Default という名称の [インストール済みNodeJS] がある場合は、この手順をスキップできます。

* [NodeJS追加] を押します
* [名前] に `Default` を入力します
* [自動インストール] をチェックします
* [バージョン] は `10.8.0` 以上を選択します
* [Apply] を押します

## Jenkins の SonarQube サーバ設定
### SonarQube の Token 生成

Web ブラウザで `https://SONARQUBE_HOSTNAME/admin/users` を開き、[Users] を表示してください。

ユーザの Tokens 列にあるボタンを押して、Tokens ポップアップを開きます。

[Enter Token Name] に `Jenkins` を入力して [Generate] ボタンを押します。

ポップアップ上に表示される Token は Jenkins で使用します。

### Jenkins の SonarQube サーバ設定

Web ブラウザで `https://JENKINS_HOSTNAME/configure` を開き、[システムの設定] を表示してください。

SonarQube servers のセクションにて以下のように設定します。

* [Enable injection of SonarQube server configuration as build environment variables] をチェックします
* [Add SonarQube] を押します
* [Name] に `Default` を入力します
* [Server URL] に `https://SONARQUBE_HOSTNAME` を入力します
* [Server authentication token] に SonarQube Token を入力します
* [Apply] を押します

## Spring Boot アプリケーション用のジョブ設定

Web ブラウザで `https://JENKINS_HOSTNAME/view/all/newJob` を開き、[新規ジョブ作成] を表示してください。

* [Enter an item name] に `spring-boot-sample-test` を入力します
* [Maven プロジェクトのビルド] を選択します
* [OK] を押します

`https://JENKINS_HOSTNAME/job/spring-boot-sample-test/configure` を開き、ジョブの設定を表示してください。

作成したジョブの設定を行います。

ソースコード管理

* [Git] を選択します
* [リポジトリURL] に `https://BITBUCKET_HOSTNAME/scm/agil/agilestack.git` を入力します
* [認証情報] に BitBucket の ID/PW を追加します
* [Apply] を押します

ビルド・トリガ

* [SCMをポーリング] をチェックします
* [Apply] を押します

ビルド環境

* [Prepare SonarQube Scanner environment] をチェックします
* [Apply] を押します

ビルド

* [ルートPOM] に `samples/spring-boot-sample/pom.xml` を入力します
* [ゴールとオプション] に `test $SONAR_MAVEN_GOAL -Dsonar.host.url=$SONAR_HOST_URL` を入力します
* [Apply] を押します

Web ブラウザで `https://JENKINS_HOSTNAME/job/spring-boot-sample-test/` を開き、ジョブを表示してください。

[ビルド実行] を押します。

もしビルド中にエラーが発生した場合は、最後に実行したビルドを開き、[コンソール出力] からエラーの原因を調べてください。

ビルド完了後、Web ブラウザで `https://SONARQUBE_HOSTNAME/projects` を開き、`spring-boot-sample` のプロジェクトを開きます。

表示したページ上でソースコードの品質チェックの結果を確認することができます。

## Nuxt.js アプリケーション用のジョブ設定

Web ブラウザで `https://JENKINS_HOSTNAME/view/all/newJob` を開き、[新規ジョブ作成] を表示してください。

* [Enter an item name] に `nuxtjs-sample-test` を入力します
* [フリースタイル・プロジェクトのビルド] を選択します
* [OK] を押します

`https://JENKINS_HOSTNAME/job/nuxtjs-sample-test/configure` を開き、ジョブの設定を表示してください。

作成したジョブの設定を行います。

ソースコード管理

* [Git] を選択します
* [リポジトリURL] に `https://BITBUCKET_HOSTNAME/scm/agil/agilestack.git` を入力します
* [認証情報] に BitBucket の ID/PW を追加します
* [Apply] を押します

ビルド・トリガ

* [SCMをポーリング] をチェックします
* [Apply] を押します

ビルド環境

* [Provide Node & npm bin/ folder to PATH] をチェックします
* [Apply] を押します

ビルド

* [ビルド手順の追加] から [シェルの実行] を追加します
* [シェルスクリプト] に以下の内容を入力します

```bash
cd samples/nuxtjs-sample
npm install
npm test
```

* [ビルド手順の追加] から [Execute SonarQube Scanner] を追加します
* [Task to run] に `scan` を入力します
* [Analysis properties] に以下の内容を入力します

```
sonar.projectKey=nuxtjs-sample
sonar.projectName=nuxtjs-sample
sonar.projectVersion=1.0
sonar.sources=samples/nuxtjs-sample
```

* [Apply] を押します

Web ブラウザで `https://JENKINS_HOSTNAME/job/nuxtjs-sample-test/` を開き、ジョブを表示してください。

[ビルド実行] を押します。

もしビルド中にエラーが発生した場合は、最後に実行したビルドを開き、[コンソール出力] からエラーの原因を調べてください。

ビルド完了後、Web ブラウザで `https://SONARQUBE_HOSTNAME/projects` を開き、`nuxtjs-sample` のプロジェクトを開きます。

表示したページ上でソースコードの品質チェックの結果を確認することができます。

## Git の Push に反応して Jenkins のジョブが実行されるように設定

Web ブラウザで `https://BITBUCKET_HOSTNAME/plugins/servlet/webhooks/projects/AGIL/repos/agilestack` を開き、
[リポジトリの設定] の [ウェブフック] の設定を表示してください。

[ウェブフックを作成する] を押して以下のように設定してください。

* [名前] に `Jenkins` を入力します
* [URL] に `https://JENKINS_HOSTNAME/git/notifyCommit?url=https://BITBUCKET_HOSTNAME/scm/agil/agilestack.git` を入力します
    * `JENKINS_HOSTNAME` と `BITBUCKET_HOSTNAME` の両方を置き換える必要があります
* [イベント] から [プッシュ] をチェックします
* [作成する] を押します

続けて BitBucket の Push に反応してジョブが実行されることをテストします。

ローカル環境にて `dummy` というファイルを作成してください。
さらに、以下のコマンドを実行して BitBucket に Push してください。

```bash
git add .
git commit -m "Add dummy"
git push agilestack master
```

Web ブラウザで `https://JENKINS_HOSTNAME/job/spring-boot-sample-test/` を開き、ジョブを表示してください。
成功のビルド履歴が追加されていれば Spring Boot アプリケーションの自動ソースコード品質チェックが成功しています。

Web ブラウザで `https://JENKINS_HOSTNAME/job/nuxtjs-sample-test/` を開き、ジョブを表示してください。
成功のビルド履歴が追加されていれば Nuxt.js アプリケーションの自動ソースコード品質チェックが成功しています。

以上でこのガイドは終了です。
