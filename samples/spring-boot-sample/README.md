# spring-boot-sample

This is sample application of Spring Boot for Agilestack.

## Test

```bash
mvn test
```

## Package

```bash
mvn package
```

## Run

Use `mvn`:

```bash
mvn spring-boot:run
```

Use `java`:

```bash
java -jar target/spring-boot-sample-1.0.0.jar
```
