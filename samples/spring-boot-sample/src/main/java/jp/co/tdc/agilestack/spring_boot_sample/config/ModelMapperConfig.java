package jp.co.tdc.agilestack.spring_boot_sample.config;

import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ModelMapperConfig {
  @Bean
  ModelMapper modelMapper() {
    return new ModelMapper();
  }
}
