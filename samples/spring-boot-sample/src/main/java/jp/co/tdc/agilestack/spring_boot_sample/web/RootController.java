package jp.co.tdc.agilestack.spring_boot_sample.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class RootController {
  @GetMapping("/")
  String index() {
    return "index";
  }
}
