package jp.co.tdc.agilestack.spring_boot_sample.domain;

import java.time.Instant;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.validation.constraints.NotNull;
import org.hibernate.annotations.GenericGenerator;
import lombok.Data;

@Entity
@Data
public class Resource {
  @Id
  @GeneratedValue(generator = "uuid2")
  @GenericGenerator(name = "uuid2", strategy = "uuid2")
  @Column(columnDefinition = "UUID")
  private UUID id;

  @NotNull
  private String name;

  @NotNull
  private String content;

  @NotNull
  private Instant createdAt;

  @NotNull
  private Instant updatedAt;

  @PrePersist
  public void onPrePersist() {
    setCreatedAt(Instant.now());
    setUpdatedAt(Instant.now());
  }

  @PreUpdate
  public void onPreUpdate() {
    setUpdatedAt(Instant.now());
  }
}
