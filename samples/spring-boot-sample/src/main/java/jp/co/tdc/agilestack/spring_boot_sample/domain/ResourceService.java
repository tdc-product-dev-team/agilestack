package jp.co.tdc.agilestack.spring_boot_sample.domain;

import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import lombok.Data;

@Service
public class ResourceService {
  @Autowired
  private ModelMapper modelMapper;

  @Autowired
  private ResourceRepository resourceRepository;

  public GetResourceOutputDto getResource(GetResourceInputDto input) {
    Optional<Resource> resource = resourceRepository.findById(input.getId());

    if (!resource.isPresent()) {
      return null;
    }

    return modelMapper.map(resource.get(), GetResourceOutputDto.class);
  }

  @Data
  public static class GetResourceInputDto {
    private UUID id;
  }

  @Data
  public static class GetResourceOutputDto {
    private UUID id;
    private String name;
    private String content;
    private Instant createdAt;
    private Instant updatedAt;
  }

  public FindResourcesOutputDto findResources(FindResourcesInputDto input) {
    List<Resource> resources = resourceRepository.findAll();
    FindResourcesOutputDto output = new FindResourcesOutputDto();

    output.setResources(modelMapper.map(resources, new TypeToken<List<FindResourcesOutputDto.ResourceDto>>() {}.getType()));

    return output;
  }

  @Data
  public static class FindResourcesInputDto {
  }

  @Data
  public static class FindResourcesOutputDto {
    private List<ResourceDto> resources;

    @Data
    public static class ResourceDto {
      private UUID id;
      private String name;
      private Instant createdAt;
      private Instant updatedAt;
    }
  }

  public CreateResourceOutputDto createResource(CreateResourceInputDto input) {
    Resource resource = modelMapper.map(input, Resource.class);

    resourceRepository.saveAndFlush(resource);

    return modelMapper.map(resource, CreateResourceOutputDto.class);
  }

  @Data
  public static class CreateResourceInputDto {
    private String name;
    private String content;
  }

  @Data
  public static class CreateResourceOutputDto {
    private UUID id;
  }

  public UpdateResourceOutputDto updateResource(UpdateResourceInputDto input) {
    Optional<Resource> optResource = resourceRepository.findById(input.getId());

    if (!optResource.isPresent()) {
      throw new ResourceNotFoundException();
    }

    Resource resource = optResource.get();

    modelMapper.map(input, resource);
    resourceRepository.saveAndFlush(resource);

    return modelMapper.map(resource, UpdateResourceOutputDto.class);
  }

  @Data
  public static class UpdateResourceInputDto {
    private UUID id;
    private String name;
    private String content;
  }

  @Data
  public static class UpdateResourceOutputDto {
    private UUID id;
  }
}
