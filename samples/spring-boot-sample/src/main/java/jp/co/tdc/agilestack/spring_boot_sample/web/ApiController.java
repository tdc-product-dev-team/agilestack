package jp.co.tdc.agilestack.spring_boot_sample.web;

import java.time.Instant;
import java.util.List;
import java.util.UUID;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.fasterxml.jackson.annotation.JsonFormat;
import jp.co.tdc.agilestack.spring_boot_sample.domain.ResourceService;
import jp.co.tdc.agilestack.spring_boot_sample.domain.ResourceService.CreateResourceInputDto;
import jp.co.tdc.agilestack.spring_boot_sample.domain.ResourceService.CreateResourceOutputDto;
import jp.co.tdc.agilestack.spring_boot_sample.domain.ResourceService.FindResourcesInputDto;
import jp.co.tdc.agilestack.spring_boot_sample.domain.ResourceService.FindResourcesOutputDto;
import jp.co.tdc.agilestack.spring_boot_sample.domain.ResourceService.GetResourceInputDto;
import jp.co.tdc.agilestack.spring_boot_sample.domain.ResourceService.GetResourceOutputDto;
import jp.co.tdc.agilestack.spring_boot_sample.domain.ResourceService.UpdateResourceInputDto;
import jp.co.tdc.agilestack.spring_boot_sample.domain.ResourceService.UpdateResourceOutputDto;
import lombok.Data;

@RestController
@RequestMapping("/api")
public class ApiController {
  private static final String TIME_FORMAT_PATTERN = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";
  private static final String TIME_FORMAT_TIMEZONE = "UTC";

  @Autowired
  private ResourceService resourceService;

  @Autowired
  private ModelMapper modelMapper;

  @GetMapping("/resources/{id}")
  GetResourceResponseDto getResource(@PathVariable("id") UUID id) {
    GetResourceInputDto input = new GetResourceInputDto();

    input.setId(id);

    GetResourceOutputDto output = resourceService.getResource(input);

    return modelMapper.map(output, GetResourceResponseDto.class);
  }

  @Data
  public static class GetResourceResponseDto {
    private UUID id;

    private String name;

    private String content;

    @JsonFormat(pattern = TIME_FORMAT_PATTERN, timezone = TIME_FORMAT_TIMEZONE)
    private Instant createdAt;

    @JsonFormat(pattern = TIME_FORMAT_PATTERN, timezone = TIME_FORMAT_TIMEZONE)
    private Instant updatedAt;
  }

  @GetMapping("/resources")
  FindResourcesResponseDto findResources() {
    FindResourcesInputDto input = new FindResourcesInputDto();
    FindResourcesOutputDto output = resourceService.findResources(input);

    return modelMapper.map(output, FindResourcesResponseDto.class);
  }

  @Data
  public static class FindResourcesResponseDto {
    private List<ResourceDto> resources;

    @Data
    public static class ResourceDto {
      private UUID id;
      private String name;

      @JsonFormat(pattern = TIME_FORMAT_PATTERN, timezone = TIME_FORMAT_TIMEZONE)
      private Instant createdAt;

      @JsonFormat(pattern = TIME_FORMAT_PATTERN, timezone = TIME_FORMAT_TIMEZONE)
      private Instant updatedAt;
    }
  }

  @PutMapping("/resources")
  CreateResourceResponseDto createResource(@RequestBody CreateResourceRequestDto request) {
    CreateResourceOutputDto output = resourceService.createResource(modelMapper.map(request, CreateResourceInputDto.class));

    return modelMapper.map(output, CreateResourceResponseDto.class);
  }

  @Data
  public static class CreateResourceRequestDto {
    private String name;
    private String content;
  }

  @Data
  public static class CreateResourceResponseDto {
    private UUID id;
  }

  @PutMapping("/resources/{id}")
  UpdateResourceResponseDto updateResource(
      @RequestBody UpdateResourceRequestDto request,
      @PathVariable("id") UUID id) {
    UpdateResourceOutputDto output = resourceService.updateResource(modelMapper.map(request, UpdateResourceInputDto.class));

    return modelMapper.map(output, UpdateResourceResponseDto.class);
  }

  @Data
  public static class UpdateResourceRequestDto {
    private String name;
    private String content;
  }

  @Data
  public static class UpdateResourceResponseDto {
    private UUID id;
  }
}
