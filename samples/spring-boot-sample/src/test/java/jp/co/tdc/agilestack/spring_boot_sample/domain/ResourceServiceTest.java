package jp.co.tdc.agilestack.spring_boot_sample.domain;

import static org.junit.Assert.*;
import org.junit.Test;
import static org.hamcrest.Matchers.*;

import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.junit4.SpringRunner;
import jp.co.tdc.agilestack.spring_boot_sample.domain.ResourceService.CreateResourceInputDto;
import jp.co.tdc.agilestack.spring_boot_sample.domain.ResourceService.CreateResourceOutputDto;

@SuppressWarnings("unused")
@RunWith(SpringRunner.class)
@SpringBootTest
public class ResourceServiceTest {
  @Autowired
  private ResourceService resourceService;

  @Test
  public void testCreateResource() {
    CreateResourceInputDto input = new CreateResourceInputDto();

    input.setName("name");
    input.setContent("content");

    CreateResourceOutputDto output = resourceService.createResource(input);

    assertThat(output.getId(), notNullValue());
  }
}
