import Vuex from 'vuex'

const store = () => new Vuex.Store({
  state: {
    counter: 0
  },
  mutations: {
    addCounter (state, counter) {
      state.counter += counter
    }
  },
  actions: {
    addCounter (context, counter) {
      context.commit('addCounter', counter)
    }
  }
})

export default store
