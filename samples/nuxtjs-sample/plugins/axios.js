export default ({ $axios }) => {
  $axios.onRequest(config => {
    config.baseURL = process.env.API_BASE_URL
  })
}
