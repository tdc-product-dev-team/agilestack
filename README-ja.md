# Agilestack

これはアジャイルプロジェクトマネジメント用のソフトウェアスタックです。

このスタックは Docker Compose によってデプロイされます。

## スタック

| サービス                 | プロダクト                                                                                                                                 |
|--------------------------|--------------------------------------------------------------------------------------------------------------------------------------------|
| プロジェクト管理         | [Jira](https://www.atlassian.com/software/jira) 7.13.1 [[Docker Image](https://hub.docker.com/r/cptactionhank/atlassian-jira-software/)]   |
| Wiki                     | [Confluence](https://www.atlassian.com/software/confluence) 6.11.0 [[Docker Image](https://hub.docker.com/r/atlassian/confluence-server/)] |
| ソースコード管理         | [BitBucket](https://www.atlassian.com/software/bitbucket) 5.12.0 [[Docker Image](https://hub.docker.com/r/atlassian/bitbucket-server/)]    |
| 継続的インテグレーション | [Jenkins](https://jenkins.io/) 2.138 [[Docker Image](https://hub.docker.com/r/jenkins/jenkins/)]                                           |
| ソースコード品質検査     | [SonarQube](https://www.sonarqube.org/) 7.1 [[Docker Image](https://hub.docker.com/_/sonarqube/)]                                          |
| コミュニケーション       | [Mattermost](https://www.mattermost.org/) 5.2.0 [[Docker Image](https://hub.docker.com/r/mattermost/mattermost-prod-app/)]                 |
| リバースプロキシ         | [Nginx](https://www.nginx.com/) 1.14 [[Docker Image](https://hub.docker.com/_/nginx/)]                                                     |
| 内部データベース         | [PostgreSQL](https://www.postgresql.org/) 10.5 [[Docker Image](https://hub.docker.com/_/postgres/)]                                        |
| 内部データベース         | [MySQL](https://www.mysql.com/) 5.7.23 [[Docker Image](https://hub.docker.com/_/mysql/)]                                                   |

## サポートされたオペレーティングシステム

* Ubuntu: =16.04
* CentOS: >=7.5.1804

## 必要なソフトウェア

* Git: >=1.8.3.1
* Docker: >=18.06.0-ce
* Docker Compose: >=1.22.0

## 必要なハードウェア
### メモリ

* 最小: 16GB

### ディスク

* 最小: 20GB

## Atlassian のソフトウェアライセンスについて

このソフトウェアスタックは無料で提供していますが、プロダクション環境にて
Atlassian のソフトウェア (Jira, Confluence, BitBucket) を利用する場合は Self-hosted ライセンスを購入する必要があります。
Atlassian は 90 日間有効なトライアルライセンスを用意していますが期限を過ぎると使用できなくなります。

* [Jira | Pricing](https://www.atlassian.com/software/jira/pricing?tab=self-hosted)
* [BitBucket | Pricing](https://bitbucket.org/product/pricing)
* [Confluence | Pricing](https://www.atlassian.com/software/confluence/pricing?tab=self-hosted)

## インストール
### 事前に必要なパッケージをインストールする

以下のパッケージをインストールしてください。

* [Git](https://git-scm.com/download/linux)
* Docker
  [[Ubuntu](https://docs.docker.com/install/linux/docker-ce/ubuntu/)]
  [[CentOS](https://docs.docker.com/install/linux/docker-ce/centos/)]
* [Docker Compose](https://docs.docker.com/compose/install/)

### 環境変数を編集する

このスタックにはいくつかの環境変数が必要です。
以下のテーブルは必要な環境変数と値の例です。

| 名前                       | 概要                                                | 値            | 例          |
|----------------------------|-----------------------------------------------------|---------------|-------------|
| JIRA_PROXY_SCHEME          | Jiraに外部アクセスする際のスキーマ                  | http or https | http        |
| JIRA_PROXY_NAME            | Jiraに外部アクセスする際のホスト名                  | any           | localhost   |
| JIRA_PROXY_PORT            | Jiraに外部アクセスする際のポート番号                | number        | 80          |
| CONFLUENCE_PROXY_SCHEME    | Confluenceに外部アクセスする際のスキーマ            | http or https | http        |
| CONFLUENCE_PROXY_NAME      | Confluenceに外部アクセスする際のホスト名            | any           | localhost   |
| CONFLUENCE_PROXY_PORT      | Confluenceに外部アクセスする際のポート番号          | number        | 80          |
| BITBUCKET_PROXY_SCHEME     | BitBucketに外部アクセスする際のスキーマ             | http or https | http        |
| BITBUCKET_PROXY_NAME       | BitBucketに外部アクセスする際のホスト名             | any           | localhost   |
| BITBUCKET_PROXY_PORT       | BitBucketに外部アクセスする際のポート番号           | number        | 80          |
| JENKINS_PROXY_SCHEME       | Jenkinsに外部アクセスする際のスキーマ               | http or https | http        |
| JENKINS_PROXY_NAME         | Jenkinsに外部アクセスする際のホスト名               | any           | localhost   |
| JENKINS_PROXY_PORT         | Jenkinsに外部アクセスする際のポート番号             | number        | 80          |
| JENKINS_JAVA_OPTS          | JenkinsのJVMパラメータ                              | any           | -Dxxx=yyy   |
| SONARQUBE_PROXY_SCHEME     | SonarQubeに外部アクセスする際のスキーマ             | http or https | http        |
| SONARQUBE_PROXY_NAME       | SonarQubeに外部アクセスする際のホスト名             | any           | localhost   |
| SONARQUBE_PROXY_PORT       | SonarQubeに外部アクセスする際のポート番号           | number        | 80          |
| MATTERMOST_PROXY_SCHEME    | Mattermostに外部アクセスする際のスキーマ            | http or https | http        |
| MATTERMOST_PROXY_WS_SCHEME | Mattermostに外部アクセスする際のWebSocketのスキーマ | ws or wss     | ws          |
| MATTERMOST_PROXY_NAME      | Mattermostに外部アクセスする際のホスト名            | any           | localhost   |
| MATTERMOST_PROXY_PORT      | Mattermostに外部アクセスする際のポート番号          | number        | 80          |
| CERTBOT_ENABLED            | Certbotを使ってプロキシのTLSを有効にする            | true or false | false       |
| CERTBOT_EMAIL              | 証明書を取得する際のメールアドレス                  | email         | xxx@yyy.zzz |
| POSTGRES_PASSWORD          | PostgreSQLのパスワード                              | any           | postgres    |
| MYSQL_PASSWORD             | MySQLのパスワード                                   | any           | mysql       |

インストールには `.env` ファイルが必要です。以下のコードは `.env` ファイルのサンプルです。

```
JIRA_PROXY_SCHEME=http
JIRA_PROXY_NAME=jira.example.com
JIRA_PROXY_PORT=80
CONFLUENCE_PROXY_SCHEME=http
CONFLUENCE_PROXY_NAME=confluence.example.com
CONFLUENCE_PROXY_PORT=80
BITBUCKET_PROXY_SCHEME=http
BITBUCKET_PROXY_NAME=bitbucket.example.com
BITBUCKET_PROXY_PORT=80
JENKINS_PROXY_SCHEME=http
JENKINS_PROXY_NAME=jenkins.example.com
JENKINS_PROXY_PORT=80
JENKINS_JAVA_OPTS=-Dorg.apache.commons.jelly.tags.fmt.timeZone=Asia/Tokyo
SONARQUBE_PROXY_SCHEME=http
SONARQUBE_PROXY_NAME=sonarqube.example.com
SONARQUBE_PROXY_PORT=80
MATTERMOST_PROXY_SCHEME=http
MATTERMOST_PROXY_WS_SCHEME=ws
MATTERMOST_PROXY_NAME=mattermost.example.com
MATTERMOST_PROXY_PORT=80
CERTBOT_ENABLED=false
CERTBOT_EMAIL=xxx@yyy.zzz
POSTGRES_PASSWORD=postgres
MYSQL_PASSWORD=mysql
```

### リバースプロキシのTLSを有効化する

環境変数の `CERTBOT_ENABLED` を `true` にすると、起動時にコンテナスクリプトが Certbot を使って証明書を取得します。

TLSを有効化する場合は以下の点に注意してください。

* 環境変数 `*_SCHEME` は `https` である必要があります。
* 環境変数 `*_PORT` は `443` である必要があります。
* コンテナはインターネットからアクセス可能である必要があります。

### コンテナをインストールする

`docker-compose` でコンテナをインストールします。

```bash
sudo docker-compose up -d
```

もしサンプルの `.env` ファイルを使っている場合は、以下のURLでサービスにアクセスすることができます。

| サービス   | 外部アクセスURL               | 内部アクセスURL       | コンテナ内URL          |
|------------|-------------------------------|-----------------------|------------------------|
| Jira       | http://jira.example.com       | http://localhost:8080 | http://jira:8080       |
| Confluence | http://confluence.example.com | http://localhost:8090 | http://confluence:8090 |
| BitBucket  | http://bitbucket.example.com  | http://localhost:7990 | http://bitbucket:7990  |
| Jenkins    | http://jenkins.example.com    | http://localhost:8081 | http://jenkins:8080    |
| SonarQube  | http://sonarqube.example.com  | http://localhost:9000 | http://sonarqube:9000  |
| Mattermost | http://mattermost.example.com | http://localhost:8000 | http://mattermost:8000 |


### ネットワークポート

| Port  | Service    | Scheme     |
|-------|------------|------------|
| 80    | Nginx      | HTTP       |
| 443   | Nginx      | HTTPS      |
| 8080  | Jira       | HTTP       |
| 8090  | Confluence | HTTP       |
| 7990  | BitBucket  | HTTP       |
| 7999  | BitBucket  | SSH        |
| 8081  | Jenkins    | HTTP       |
| 9000  | SonarQube  | HTTP       |
| 8000  | Mattermost | HTTP       |
| 5432  | PostgreSQL | PostgreSQL |

### Jira をセットアップする

Web ブラウザから Jira の URL にアクセスし、インストールウィザードを表示してください。

プロダクション向けインストールを選択してください。

データベース接続設定は以下のように入力してください。

| 項目               | 値                                 |
|--------------------|------------------------------------|
| データベースの種類 | `PostgreSQL`                       |
| ホスト名           | `postgres`                         |
| ポート番号         | `5432`                             |
| データベース名     | `jira`                             |
| ユーザ名           | `postgres`                         |
| パスワード         | `POSTGRES_PASSWORD` の環境変数の値 |

インストールウィザードに従い、残りの設定を行ってください。

### Confluence をセットアップする

Web ブラウザから Confluence の URL にアクセスし、インストールウィザードを表示してください。

プロダクション向けインストールを選択してください。

データベース接続設定は以下のように入力してください。

| 項目               | 値                                 |
|--------------------|------------------------------------|
| データベースの種類 | `PostgreSQL`                       |
| ホスト名           | `postgres`                         |
| ポート番号         | `5432`                             |
| データベース名     | `confluence`                       |
| ユーザ名           | `postgres`                         |
| パスワード         | `POSTGRES_PASSWORD` の環境変数の値 |

インストールウィザードに従い、残りの設定を行ってください。

### BitBucket をセットアップする

Web ブラウザから BitBucket の URL にアクセスし、インストールウィザードを表示してください。

プロダクション向けインストールを選択してください。

データベース接続設定は以下のように入力してください。

| 項目               | 値                                 |
|--------------------|------------------------------------|
| データベースの種類 | `PostgreSQL`                       |
| ホスト名           | `postgres`                         |
| ポート番号         | `5432`                             |
| データベース名     | `bitbucket`                        |
| ユーザ名           | `postgres`                         |
| パスワード         | `POSTGRES_PASSWORD` の環境変数の値 |

インストールウィザードに従い、残りの設定を行ってください。

### Jenkins をセットアップする

Web ブラウザから Jenkins の URL にアクセスし、インストールウィザードを表示してください。

Jenkins のインストールには初期管理パスワードが必要です。
以下のコマンドで初期管理パスワードを表示します。

```bash
sudo docker exec -it $(sudo docker ps -f "name=jenkins" --format "{{.ID}}") cat /var/jenkins_home/secrets/initialAdminPassword
```

インストールウィザードに従い、残りの設定を行ってください。

### SonarQube をセットアップする

Web ブラウザから Jenkins の URL にアクセスし、ホームページを表示してください。

初期パスワードは重大なセキュリティリスクです。最初のログイン後にパスワードを必ず変更してください。
SonarQube の 初期 ID/PW は admin/admin です。

さらに[ユーザ認証の強制(Force user authentication)を有効化](https://docs.sonarqube.org/display/SONAR/Authentication)してください。

## 使い方

使い方は [ガイド](docs/guides/ja) を参照してください。
