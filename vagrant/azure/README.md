# Vagrantfile for Microsoft Azure

This is Vagrantfile, provision a *avileenv* server machine in Microsoft Azure.

## Prerequires

* Install [Vagrant](https://www.vagrantup.com/downloads.html)
* Sign up [Microsoft Azure](https://azure.microsoft.com)
* Install [Azure CLI](https://docs.microsoft.com/en-us/cli/azure/install-azure-cli)

## Install Vagrant Azure Provider

Run following commands to install [Vagrant Azure Provider](https://github.com/Azure/vagrant-azure):

```
vagrant box add azure https://github.com/azure/vagrant-azure/raw/v2.0/dummy.box --provider azure
vagrant plugin install vagrant-azure
```

## Create an Azure Active Directory (AAD) Application

Run `az login` to log into Azure.
The output should look like the following:

```
[
  {
    "cloudName": "AzureCloud",
    "id": "<AZURE_SUBSCRIPTION_ID>",
    "isDefault": true,
    "name": "XXXXXXXXXX",
    "state": "Enabled",
    "tenantId": "<AZURE_TENANT_ID>",
    "user": {
      "name": "xxxxx@xxxxx",
      "type": "user"
    }
  },
  ...
]
```

Copy your subscription ID and run `az account set --subscription <AZURE_SUBSCRIPTION_ID>`.

Run `az ad sp create-for-rbac` to create an Azure Active Directory Application.
The output should look like the following:

```
{
  "appId": "<AZURE_CLIENT_ID>",
  "displayName": "XXXXXXXXXX",
  "name": "http://XXXXXXXXXX",
  "password": "<AZURE_CLIENT_SECRET>",
  "tenant": "<AZURE_TENANT_ID>"
}
```

Run `az group list` to show *Resource Groups*.
The output should look like the following:

```
[
  {
    "id": "/subscriptions/ffffffff-ffff-ffff-ffff-ffffffffffff/resourceGroups/xxxxxxxxxx",
    "location": "westus2",
    "managedBy": null,
    "name": "<AZURE_RESOURCE_GROUP_NAME>",
    "properties": {
      "provisioningState": "Succeeded"
    },
    "tags": null
  },
  ...
]
```

## Create keypair files

Create keypair for logging into Azure Virtual Machine.
If your local machine installed OpenSSL then run following command:

```
ssh-keygen -t rsa -b 2048
```

## Environment variables

Vagrantfile requires following environment variables.

| Name                      | Description                                                         |
|---------------------------|---------------------------------------------------------------------|
| PRIVATE_KEY_PATH          | Private key file for logging into with SSH                          |
| AZURE_TENANT_ID           | Tenant ID appeared in `az ad sp create-for-rbac`                    | 
| AZURE_CLIENT_ID           | AAD application client ID appered in `az ad sp create-for-rbac`     | 
| AZURE_CLIENT_SECRET       | AAD application client secret appered in `az ad sp create-for-rbac` | 
| AZURE_SUBSCRIPTION_ID     | Subscription ID appered in `az login`                               |
| AZURE_VM_NAME             | Virtual machine name, DNS label prefix, Resource group name         |
| AZURE_LOCATION            | Deployment location (e.g. westus2)                                  |

For example:

```bash
export PRIVATE_KEY_PATH=id_rsa
export AZURE_TENANT_ID=ffffffff-ffff-ffff-ffff-ffffffffffff
export AZURE_CLIENT_ID=ffffffff-ffff-ffff-ffff-ffffffffffff
export AZURE_CLIENT_SECRET=xxxxxxxxxx
export AZURE_SUBSCRIPTION_ID=ffffffff-ffff-ffff-ffff-ffffffffffff
export AZURE_VM_NAME=agilestack
export AZURE_LOCATION=westus2
```

```bat
SET PRIVATE_KEY_PATH=id_rsa
SET AZURE_TENANT_ID=ffffffff-ffff-ffff-ffff-ffffffffffff
SET AZURE_CLIENT_ID=ffffffff-ffff-ffff-ffff-ffffffffffff
SET AZURE_CLIENT_SECRET=xxxxxxxxxx
SET AZURE_SUBSCRIPTION_ID=ffffffff-ffff-ffff-ffff-ffffffffffff
SET AZURE_VM_NAME=agilestack
SET AZURE_LOCATION=westus2
```

## Edit environment variable file for Docker Compose

If `ansible/.env` file exists then Ansible script will copy to `/usr/local/src/agilestack/.env`.

Read [README.md](../../README.md) and edit `ansible/.env` file.

## Deployment

* Run `vagrant up --provider=azure` to deploy
* Run `vagrant ssh` to log into virtual machine
* Run `cd /usr/local/src/agilestack` to change current directory

Read [README.md](../../README.md) and setup *Agilestack*.
